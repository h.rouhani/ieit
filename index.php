<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>مهندسی صنایع | فناوری اطلاعات</title>

<style>
body {
  margin: 0;
  font-size: 28px;
}

.header {
  background-color: #f1f1f1;
  padding: 30px;
  text-align: center;
}

#navbar {
  overflow: hidden;
  background-color: #333;
}

#navbar a {
  float: right;
  display: block;
  color: #fff;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%
}

.sticky + .content {
  padding-top: 60px;
}
</style>
</head>
<body onscroll="myFunction()">

<div class="header">
<img src="main/Logo.png"/>
  <h2><span style="font-weight: 400"><font face="Tahoma" size="6">مهندسی 
	صنایع | فناوری اطلاعات</font></span></h2>
<p><font face="Tahoma" size="3">حامد روحانی</font></div>
 
<div id="navbar">
  <font face="Tahoma">
  <a class="active" href="index.php"><font size="4">صفحه اصلی</font></a>
  <a href="main/destination.php?job=view&cat=IE"><font size="4">مهندسی صنایع</font></a>
  <a href="main/destination.php?job=view&cat=IT"><font size="4">فناوری اطلاعات</font></a>
  <a href="main/destination.php?job=view&cat=Both"><font size="4">مباحث مشترک</font></a>
  <a href=""><font size="4"> تماس با ما</font></a>
<?php
session_start();
  if(!empty($_SESSION["level"])) {
    echo '<a href="main/editor/editor.php"><font size="4">ویرایشگر</font></a>
          <a href="main/editor/assort.php"><font size="4">طبقه بندی مطالب</font></a>';
  }
  if(empty($_SESSION["userName"])) {
    echo '<a href="main/login/login.html"><font size="4">ورود به سایت</font></a>';
  }else{
  echo '<a href="main/destination.php?job=exit"><font size="4">خروج</font></a>';
  }
?>
          
</div> 
<br />
<div class="content">
	<p align="right" dir="rtl"><font size="5" face="Tahoma">&nbsp;</font><font face="Tahoma" size="4">با 
	توجه به پیشرفت روز افزون 
	فناوری اطلاعات، دنیای مهندسی صنایع با سرعتی قابل ملاحظه در حال تغییر می 
	باشد. لذا با توجه به این مساله، نیاز بیشتری به تخصصی میانه احساس می شود.</font></p>
	<p align="right" dir="rtl"><font face="Tahoma" size="4">این سایت با هدف فوق ایجاد شده 
	و سعی بر این است که به مرور زمان،&nbsp; هر چه بیشتر از ابزارهای
	<span lang="en-us">IT</span> در روشهای مهندسی صنایع استفاده گردد.</font></p>
	<p align="right" dir="rtl">&nbsp;</p>
	<p align="right" dir="rtl">&nbsp;</p>
	<p align="right" dir="rtl">&nbsp;</p>
	<p align="right" dir="rtl">&nbsp;</p>
	<p align="right" dir="rtl">&nbsp;</p>
	<p align="right" dir="rtl">&nbsp;</p>
	<p align="right" dir="rtl">&nbsp;</p>
  <p align="right">
</div>

<script>
var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>
    
</body>
</html> 