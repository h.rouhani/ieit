<?php
include_once "publics.php";

include_once "converter.php";

$job = $_REQUEST['job'];

session_start();

//login to www.ieit.ir
if ($job == "login") {
    $conn = getDBConnection();
    $username = $_REQUEST['userName'];
    $password = $_REQUEST['Password'];
    $password = md5($password);
    $code = $_REQUEST['code'];
    $Counter = 0;
    if (empty($conn))
        die("error in app");
    else {
        if ($code != $_SESSION['captcha']){
            header("refresh:2;url=login/login.html");
            echo <<<eof
            <html>
            <body>
            <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#CCFFCC" align="center">
                    <p dir="rtl"><b>
					<font color="#CC3300" face="Tahoma" size="4">&nbsp;&#1705;&#1583; &#1575;&#1605;&#1606;&#1740;&#1578;&#1740;</font><font size="4" face="Tahoma"> &#1575;&#1588;&#1578;&#1576;&#1575;&#1607; 
					&#1608;&#1575;&#1585;&#1583; &#1588;&#1583;&#1607; &#1575;&#1587;&#1578;.</font></b></td>
                </tr>
            </table>
            </div>
            </body>
            </html>
eof;
        }else{
        mysqli_query($conn,'SET NAMES utf8');
            $results = mysqli_query($conn, "SELECT * FROM users WHERE IsDel = 0");
        if (mysqli_num_rows($results) > 0) {
            while ($row = mysqli_fetch_assoc($results)) {
                $DBusername = $row['Email'];
                $DBpassword = $row['password'];
                if ($username == $DBusername && $password == $DBpassword) {
                    $Counter = 1;
                    $setUsername = $row["Email"];
                    $setLevel = $row["level"];
                }
            }
        if ($Counter==1){
            $_SESSION['userName'] = $setUsername;
            if($setLevel==902){
                $_SESSION['level'] = $setLevel;
            }
            header("refresh:2;url=../index.php");
            echo <<<eof
            <html>
            
			<head>
			<meta http-equiv="Content-Language" content="fa">
			</head>

            <body>
            <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#CCFFCC" align="center">
                    <p dir="rtl"><b>
					<font face="Tahoma" size="4">&#1705;&#1575;&#1585;&#1576;&#1585; &#1711;&#1585;&#1575;&#1605;&#1740;
                    &#1548; &#1582;&#1608;&#1588; &#1570;&#1605;&#1583;&#1740;&#1583; ...</font></b>
                    </td>
                </tr>
            </table>
            </div>
            </body>
            </html>
eof;
               
        }else{
            header("refresh:2;url=login/login.html");
            echo <<<eof
            <html>
            <body>
            <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#CCFFCC" align="center">
                    <p dir="rtl"><b><font color="#CC3300" face="Tahoma">
					<font size="4">&#1606;&#1575;&#1605; &#1705;&#1575;&#1585;&#1576;&#1585;&#1740;
					</font></font><font size="4"><font face="Tahoma">&#1740;&#1575;</font><font color="#CC3300" face="Tahoma"> 
					&#1585;&#1605;&#1586; &#1593;&#1576;&#1608;&#1585;</font></font><font size="4" face="Tahoma"> &#1575;&#1588;&#1578;&#1576;&#1575;&#1607; &#1575;&#1587;&#1578;.</font></b></td>
                </tr>
            </table>
            </div>
            </body>
            </html>
eof;

        }        
        }
    mysqli_close($conn);
    }
    }
}

//register in www.ieit.ir
if ($job == "register") {
    $conn = getDBConnection();
    $Name = $_REQUEST['Name'];
    $Family = $_REQUEST['family'];
    $Mobile = $_REQUEST['mobile'];
    $Email = $_REQUEST['email'];
    $Gender = $_REQUEST['gender'];
    $newPassword = $_REQUEST['newPassword'];
    $rePassword = $_REQUEST['rePassword'];
    $Counter = 0;
    if (empty($conn))
        die("error in app");
    else {
        if( filter_var($Email, FILTER_VALIDATE_EMAIL) and !empty($Email) ){
            $results = mysqli_query($conn, "SELECT * FROM users WHERE email='".$Email."'");
        if (mysqli_num_rows($results) > 0) {
            mysqli_close($conn);
            header("refresh:2;url=login/register.htm");
            echo <<<eof
            <html>
            
			<head>
			<meta http-equiv="Content-Language" content="fa">
			</head>

            <body>
            <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#FFCCFF" align="center">
                    <p dir="rtl"><b><font face="Tahoma" size="4">&#1570;&#1583;&#1585;&#1587; email &#1602;&#1576;&#1604;&#1575; &#1608;&#1575;&#1585;&#1583; &#1588;&#1583;&#1607; &#1575;&#1587;&#1578; 
					!!!</font></b>
                    </td>
                </tr>
            </table>
            </div>
            </body>
            </html>
eof;
        }else{
            if (strlen($newPassword)< 5 or strlen($rePassword)< 5 ){
                header("refresh:2;url=login/register.htm");
                echo <<<eof
                <html>
            
                <head>
                    <meta http-equiv="Content-Language" content="fa">
                </head>

                <body>
                <div>
                    <table border="0" width="374" height="133" align="center">
                        <tr>
                            <td width="368" bgcolor="#FFCCFF" align="center">
                            <p dir="rtl"><b><font face="Tahoma" size="4">&#1585;&#1605;&#1586; 
							&#1608;&#1585;&#1608;&#1583; &#1576;&#1575;&#1740;&#1583; &#1576;&#1740;&#1588;&#1578;&#1585; &#1575;&#1586; <font color="#CC3300">5</font> 
							&#1705;&#1575;&#1585;&#1575;&#1705;&#1578;&#1585; &#1576;&#1575;&#1588;&#1583; ... </font></b>
							<p dir="rtl"><b><font face="Tahoma" size="4">&#1604;&#1591;&#1601;&#1575; 
							&#1583;&#1602;&#1578; &#1576;&#1601;&#1585;&#1605;&#1575;&#1740;&#1740;&#1583;.</font></b></td>
                        </tr>
                    </table>
                </div>
                </body>
                </html>
eof;
            }else{
                if ($newPassword==$rePassword){
                    $conn = getDBConnection();
                    $newPassword = md5($newPassword);
                    if (empty($conn))
                        die("error in app");
                    else {
                        $sql = "INSERT INTO users
                                   ( Email ,Password , emailStatus , Name, Family, Gender, Mobile )
                            VALUES ('$Email' , '$newPassword' , '0' , '$Name','$Family','$Gender','$Mobile')";
                    mysqli_query($conn, $sql);
                    mysqli_close($conn);
                    $_SESSION['userName'] = $Email;
                    header("refresh:2;url=../index.php");
                    echo <<<eof
                         <html>
 			             <head>
			             <meta http-equiv="Content-Language" content="fa">
			             </head>

                        <body>
                        <div>
                            <table border="0" width="374" height="133" align="center">
                                <tr>
                                    <td width="368" bgcolor="#CCFFCC" align="center">
                                    <p dir="rtl"><b><font face="Tahoma" size="4">&#1570;&#1583;&#1585;&#1587;
					                   <span lang="en-us">email</span></font></b><b><font face="Tahoma" size="4"> 
					                   &#1588;&#1605;&#1575; &#1576;&#1575; &#1605;&#1608;&#1601;&#1602;&#1740;&#1578; &#1579;&#1576;&#1578; &#1588;&#1583;.</font></b><p dir="rtl"><b>
					                   <font face="Tahoma" size="4">&#1705;&#1575;&#1585;&#1576;&#1585; &#1711;&#1585;&#1575;&#1605;&#1740;
                                        &#1548; &#1582;&#1608;&#1588; &#1570;&#1605;&#1583;&#1740;&#1583; ...</font></b>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        </body>
                        </html>
eof;
                    } 
                }else{
                header("refresh:2;url=login/register.htm");
                echo <<<eof
                 <html>
            
			     <head>
			     <meta http-equiv="Content-Language" content="fa">
			     </head>

                <body>
                <div>
                    <table border="0" width="374" height="133" align="center">
                        <tr>
                            <td width="368" bgcolor="#FFCCFF" align="center">
                            <p dir="rtl"><b><font face="Tahoma" size="4">&#1583;&#1585; &#1578;&#1705;&#1585;&#1575;&#1585;
                            <font color="#CC3300">&#1585;&#1605;&#1586; &#1608;&#1585;&#1608;&#1583;</font> &#1583;&#1602;&#1578; &#1576;&#1601;&#1585;&#1605;&#1575;&#1740;&#1740;&#1583; !!!</font></b></td>
                        </tr>
                    </table>
                </div>
                </body>
                </html>
eof;
                }
            }
        }
    }else{
        header("refresh:2;url=login/register.htm");
        echo <<<eof
        <html>
            
			<head>
			<meta http-equiv="Content-Language" content="fa">
			</head>

            <body>
            <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#FFCCFF" align="center">
                    <p dir="rtl"><b><font face="Tahoma" size="4">&#1570;&#1583;&#1585;&#1587; email 
					&#1575;&#1588;&#1578;&#1576;&#1575;&#1607; &#1608;&#1575;&#1585;&#1583; &#1588;&#1583;&#1607; &#1575;&#1587;&#1578; 
					!!!</font></b>
                    </td>
                </tr>
            </table>
            </div>
            </body>
            </html>
eof;
    }
    }
}

//logout from www.ieit.ir
if ($job == "exit") {
    
    unset($_SESSION['userName']);
    unset($_SESSION['level']);
    unset($_SESSION['captcha']);

    header("refresh:2;url=../index.php");
    echo <<<eof
            <html>
            
			<head>
			<meta http-equiv="Content-Language" content="fa">
			</head>

            <body>
            <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#CCFFCC" align="center">
                    <p dir="rtl"><b><font face="Tahoma" size="4">&#1593;&#1605;&#1604;&#1740;&#1575;&#1578; &#1582;&#1585;&#1608;&#1580; &#1588;&#1605;&#1575; 
					&#1576;&#1575; &#1605;&#1608;&#1601;&#1602;&#1740;&#1578; &#1575;&#1606;&#1580;&#1575;&#1605; &#1588;&#1583; ...</font></b>
                    </td>
                </tr>
            </table>
            </div>
            </body>
            </html>
eof;
}

//new category and title set
if ($job == "newAssort") {
    $conn = getDBConnection();
    if (empty($conn))
        die("error in app");
    else {
        $catName = $_POST["catName"];
        $title = $_POST["title"];
        $editUser = $_SESSION["userName"];
        $sql = "INSERT INTO assort 
                                   ( catName ,title , editUser)
                            VALUES ('$catName' , '$title' , '$editUser')";
        mysqli_query($conn, $sql);
        mysqli_close($conn);
        header("refresh:0.2;url=editor/assort.php");
    }
}

//edit category and title set
if ($job == "editAssort") {
    $conn = getDBConnection();
    if (empty($conn))
        die("error in app");
    else {
        $catName = $_POST["catName"];
        $title = $_POST["title"];
        $editUser = $_SESSION["userName"];
        $ID =$_POST["assortID"];
        $sql = "UPDATE assort  SET catName ='$catName' ,title='$title' , editUser='$editUser'
                WHERE ID ='$ID'";
        mysqli_query($conn, $sql);
        mysqli_close($conn);
        header("refresh:0.2;url=editor/assort.php");
    }
}

//delete category and title set
if ($job == "delAssort") {
    $conn = getDBConnection();
    if (empty($conn))
        die("error in app");
    else {
        $editUser = $_SESSION["userName"];
        $ID =$_POST["assortID"];
        $sql = "UPDATE assort  SET editUser='$editUser', isDel=1
                WHERE ID ='$ID'";
        mysqli_query($conn, $sql);
        mysqli_close($conn);
        header("refresh:0.2;url=editor/assort.php");
    }
}

//adding an article to Database
if ($job == "addArticle") {
    $conn = getDBConnection();
    $catID = $_REQUEST['catID'];
    $title = $_REQUEST['title'];
    $content = $_REQUEST['content'];
    $editUser = $_SESSION['userName'];
    mysqli_query($conn, "INSERT INTO articles (title , content, catID,  editUser )
                                        VALUES( '$title' , '$content','$catID' , '$editUser')");
    header("refresh:2;url=../index.php");
    echo <<<EOF
    <html>
    <head>
	<meta http-equiv="Content-Language" content="fa">
	</head>
    <body>
        <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#CCFFCC" align="center">
                    <p dir="rtl"><b><font face="Tahoma" size="4">&#1605;&#1578;&#1606; &#1580;&#1583;&#1740;&#1583;
					&#1576;&#1575; &#1605;&#1608;&#1601;&#1602;&#1740;&#1578;
					&#1579;&#1576;&#1578; &#1711;&#1585;&#1583;&#1740;&#1583; ...</font></b>
                    </td>
                </tr>
            </table>
        </div>
     </body>
     </html>
EOF;
mysqli_close($conn);
}

//Editing an article
if ($job == "editArticle") {
    $conn = getDBConnection();
    $catID = $_REQUEST['catID'];
    $title = $_REQUEST['title'];
    $content = $_REQUEST['content'];
    $editUser = $_SESSION['userName'];
    $articleID = $_REQUEST['articleID'];
    $sql = "UPDATE articles SET title ='$title', content = '$content', catID = '$catID' , editUser ='$editUser'
    WHERE ID = '$articleID'";
    mysqli_query($conn,$sql);
    header("refresh:2;url=destination.php?job=view&catID=$catID");
    echo <<<EOF
    <html>
    <head>
    <meta http-equiv="Content-Language" content="fa">
    </head>
    <body>
        <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#CCFFCC" align="center">
                    <p dir="rtl"><b><font face="Tahoma" size="4">&#1605;&#1578;&#1606; &#1576;&#1575; &#1605;&#1608;&#1601;&#1602;&#1740;&#1578;
                    &#1608;&#1740;&#1585;&#1575;&#1740;&#1588; &#1711;&#1585;&#1583;&#1740;&#1583; ...</font></b>
                    </td>
                </tr>
            </table>
        </div>
     </body>
     </html>
EOF;
mysqli_close($conn);
}

//Deleting an article
if ($job == "deleteArticle") {
    $conn = getDBConnection();
    $catID = $_REQUEST['catID'];
    $editUser = $_SESSION['userName'];
    $articleID = $_REQUEST['articleID'];
    $sql = "UPDATE articles SET IsDel =1 , editUser ='$editUser'
    WHERE ID = '$articleID'";
    mysqli_query($conn,$sql);
    header("refresh:2;url=destination.php?job=view&catID=$catID");
    echo <<<EOF
    <html>
    <head>
    <meta http-equiv="Content-Language" content="fa">
    </head>
    <body>
        <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#CCFFCC" align="center">
                    <p dir="rtl"><b><font face="Tahoma" size="4">&#1605;&#1578;&#1606; &#1576;&#1575; &#1605;&#1608;&#1601;&#1602;&#1740;&#1578;
                    &#1581;&#1584;&#1601; &#1711;&#1585;&#1583;&#1740;&#1583; ...</font></b>
                    </td>
                </tr>
            </table>
        </div>
     </body>
     </html>
EOF;
mysqli_close($conn);
}

// Article views
if ($job=="view"){
    
    if(empty($_REQUEST["cat"])){
        $catName = $_SESSION['catName'];
        $catID = $_REQUEST['catID'];
        $sqlArticles = "SELECT * FROM assort INNER JOIN articles
                            WHERE articles.catID = assort.ID AND assort.ID = '$catID'
                                AND left(assort.title,4) != 'main' AND articles.IsDel = 0
                            ORDER BY articles.editTime";
    }else{
        $catName = $_REQUEST["cat"];
        $_SESSION['catName'] = $catName;
        $sqlArticles = "SELECT * FROM assort INNER JOIN articles 
                            WHERE articles.catID = assort.ID AND assort.catName = '$catName' 
                                AND left(assort.title,4) = 'main' AND articles.IsDel = 0
                            ORDER BY articles.editTime";
    }
    
    if ($catName == "IT"){
            $topic = "&#1601;&#1606;&#1575;&#1608;&#1585;&#1740; &#1575;&#1591;&#1604;&#1575;&#1593;&#1575;&#1578;";
        }elseif($catName == "IE"){
            $topic = "&#1605;&#1607;&#1606;&#1583;&#1587;&#1740; &#1589;&#1606;&#1575;&#1740;&#1593;";
        }elseif($catName == "Both"){
            $topic = "&#1605;&#1576;&#1575;&#1581;&#1579; &#1605;&#1588;&#1578;&#1585;&#1705;";
        }
    $_SESSION['topic'] = $topic;
        
    $conn = getDBConnection();
            
    if (empty($conn))
        die("error in app");
    else {
        
        $sqlMenu = "SELECT * FROM assort WHERE isDel =0 AND catName ='$catName' AND left(title,4) != 'Main' GROUP BY title ORDER BY title";
        
        $results = mysqli_query($conn, $sqlMenu);
        $Counter = 0;
        $stackMenuTitle = array();
        $stackID = array();
        if (mysqli_num_rows($results) > 0) {
            while ($row = mysqli_fetch_assoc($results)) {
                $stackMenuTitle[$Counter] = $row["title"];
                $stackID[$Counter] = $row["ID"];
                $Counter++;
            }
        }
        $_SESSION['stackMenuTitle'] = $stackMenuTitle;
        $_SESSION['stackID'] = $stackID;
      
          
            $results = mysqli_query($conn, $sqlArticles);
            
            $Counter = 0;
            $stackArticleID = array();
            $stackTitle = array();
            $stackContent = array();
            $stackCalendar = array();
            $stackHour = array();
            
            if (mysqli_num_rows($results) > 0) {
                while ($resultArray = mysqli_fetch_assoc($results)) {
                    $editTime = $resultArray['editTime'];
                    $datetime = new DateTime($editTime);
                    $year = $datetime->format('Y');
                    $month = $datetime->format('m');
                    $day = $datetime->format('d');
                    $jalali = gregorian_to_jalali($year, $month, $day);


                    $stackCalendar[$Counter] = $jalali[0] . "/" . $jalali[1] . "/" . $jalali[2];
                    $stackHour[$Counter] = substr($editTime,11);
                    $stackArticleID[$Counter] = $resultArray['ID'];
                    $stackTitle[$Counter] = $resultArray['title'];
                    $stackContent[$Counter] = $resultArray['content'];
                    $Counter++;
                }
            }
            $_SESSION['stackTitle'] = $stackTitle;
            $_SESSION['stackContent'] = $stackContent;
            $_SESSION['stackCalendar'] = $stackCalendar;
            $_SESSION['stackHour'] = $stackHour;
            $_SESSION['stackArticleID'] = $stackArticleID;

    }
    mysqli_close($conn);
    header("refresh:0;url=view.php");
}

//new comment set for an article
if ($job == "commentArticle") {
    $conn = getDBConnection();
    if (empty($conn))
        die("error in app");
    else {
        $catID = $_POST["catID"];
        $articleID = $_POST["articleID"];
        $subject = $_POST["subject"];
        $comment = $_POST["comment"];
        $editUser = $_SESSION["userName"];
        
        if (empty($subject) or empty($comment)){
            header("refresh:3;url=editor/comment.php?articleID=$articleID");
    echo <<<EOF
    <html>
    <head>
    <meta http-equiv="Content-Language" content="fa">
    </head>
    <body>
        <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#CCFFCC" align="center">
                    <p dir="rtl"><b><font face="Tahoma" size="4">&#1604;&#1591;&#1601;&#1575; &#1583;&#1585; &#1608;&#1585;&#1608;&#1583;
					<font color="#800000">&#1605;&#1608;&#1590;&#1608;&#1593;</font> &#1740;&#1575; <font color="#800000">
					&#1605;&#1578;&#1606;</font> &#1583;&#1602;&#1578; &#1576;&#1601;&#1585;&#1605;&#1575;&#1574;&#1740;&#1583; ...</font></b><p dir="rtl"><b>
					<font face="Tahoma" size="4">&#1604;&#1575;&#1586;&#1605; &#1575;&#1587;&#1578; &#1575;&#1740;&#1606; &#1583;&#1608; &#1605;&#1608;&#1585;&#1583; &#1608;&#1575;&#1585;&#1583; &#1588;&#1583;&#1607; 
					&#1576;&#1575;&#1588;&#1606;&#1583;...</font></b>
                    </td>
                </tr>
            </table>
        </div>
     </body>
     </html>
EOF;
        }else{
        $sql = "INSERT INTO comment 
                                   ( catID ,articleID , subject, comment,editUser , isDel)
                            VALUES ('$catID' , '$articleID' ,'$subject','$comment', '$editUser',0)";
        mysqli_query($conn, $sql);
        mysqli_close($conn);
        header("refresh:2;url=view.php");
    echo <<<EOF
    <html>
    <head>
    <meta http-equiv="Content-Language" content="fa">
    </head>
    <body>
        <div>
            <table border="0" width="374" height="133" align="center">
                <tr>
                    <td width="368" bgcolor="#CCFFCC" align="center">
                    <p dir="rtl"><b><font face="Tahoma" size="4">&#1606;&#1592;&#1585; &#1575;&#1585;&#1587;&#1575;&#1604; &#1588;&#1583;&#1607; &#1576;&#1575; &#1605;&#1608;&#1601;&#1602;&#1740;&#1578;
                    &#1579;&#1576;&#1578; &#1711;&#1585;&#1583;&#1740;&#1583; ...</font></b>
                    </td>
                </tr>
            </table>
        </div>
     </body>
     </html>
EOF;
        }
    }
}
?>