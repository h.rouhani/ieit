<html dir="rtl">
<head lang="fa">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>IEIT</title>

<style>
body {
  margin: 0;
  font-size: 28px;
}

.header {
  background-color: #f1f1f1;
  padding: 30px;
  text-align: center;
}

#navbar {
  overflow: hidden;
  background-color: #333;
}

#navbar a {
  float: right;
  display: block;
  color: #fff;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%
}

.sticky + .content {
  padding-top: 60px;
}
</style>
</head>
<body onscroll="myFunction()">
<?php
    session_start();
    
    $topic = $_SESSION['topic'];
    echo '<h1 align="center">'.$topic.'</h1>';
?>
<div id="navbar">
  <font face="Tahoma">
  <a href="../index.php" dir="rtl"><font size="4">&#1589;&#1601;&#1581;&#1607; &#1575;&#1589;&#1604;&#1740;</font></a>
<?php
    $stackMenuTitle = $_SESSION['stackMenuTitle'] ;
    $stackID = $_SESSION['stackID'];
    
    for ($x = 0; $x < count($stackID); $x++) {
        echo '<a href="destination.php?job=view&catID='.$stackID[$x].'&cat=""">
        <font size="4">'.$stackMenuTitle[$x].'</font></a>';
    }
?>
</div> 

<br/>
<div class="content">
            <div align="right">
			<table >
<?php
            $stackTitle = $_SESSION['stackTitle']; 
            $stackContent = $_SESSION['stackContent'];
            $stackCalendar = $_SESSION['stackCalendar'];
            $stackHour = $_SESSION['stackHour'];
            $stackArticleID = $_SESSION['stackArticleID'];
            
            for ($x = 0; $x < count($stackHour); $x++) {
?>
            	<tr>
					<td width="10%"><font face="Tahoma"><b>عنوان:</b></td>
					<td><b><?php echo $stackTitle[$x]; ?></b></td>
					<td > &nbsp;</td>
                    <td > &nbsp;</td>
				</tr>
				<tr>
					<td colspan="4" bgcolor="#FFFFCC"><?php echo $stackContent[$x]; ?></td>
				</tr>
                <tr>
					<td width="10%"><font face="Tahoma">
					    <span style="font-size: 10pt"><span style="color: #B2280C">تاریخ:
                        </span></span></td>
					<td><span style="font-size: 10pt"><span style="color: #B2280C">
					    <?php echo $stackCalendar[$x]; ?></span></span></td>
					<td width="10%"><font face="Tahoma">
					    <span style="font-size: 10pt"><span style="color: #B2280C">ساعت:
                        </span></span>
                    </td>
					<td><span style="font-size: 10pt"><span style="color: #B2280C">
					    <?php echo $stackHour[$x]; ?></span></span></td>
				</tr>
				<tr>
					<td>
					    <?php if(!empty($_SESSION["level"])) {  ?>
                        <form method="post" action="editor/editorEdit.php">
                            <input type="submit" value="ویرایش"name="sbmEdit"/>
                            <input type="hidden" value="<?php echo $stackArticleID[$x]; ?>" name="articleID" />
                        </form>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if(!empty($_SESSION["level"])) {  ?>
                        <form method="post" action="destination.php">
                            <input type="submit" value="حذف"name="sbmDel"/>
                            <input type="hidden" value="<?php echo $stackArticleID[$x]; ?>" name="articleID" />
                            <input type="hidden" value="deleteArticle" name="job" />
                            <input type="hidden" value="" name="catID" />
                        </form>
                        <?php } ?>
                    </td>
                    <td colspan="2">
                        <?php if(!empty($_SESSION["userName"])) {  ?>
                        <form method="post" action="editor/comment.php">
                            <input type="submit" value="ارسال نظر"name="sbmComment"/>
                            <input type="hidden" value="<?php echo $stackArticleID[$x]; ?>" name="articleID" />
                            <input type="hidden" value="articlecomment" name="job" />
                        </form>
                        <?php }else{
                            echo "<span style='font-size: 10pt'><span style='color: #B2280C'>لطفا برای ارسال نظر به سایت وارد شوید</span></span>";} ?>
                    </td>
				</tr>

<?php
            }
?>
			</table>
			</div>


</div>

<script>
var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>
   
</body>
</html>'

 
