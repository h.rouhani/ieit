<html dir="rtl">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>طبقه بندی مطالب</title>
</head>

<?php
include_once "../publics.php";

session_start();

if(!isset($_SESSION["userName"])) {
    header("refresh:0;url=../login/login.html");
}else{
    if(!isset($_SESSION["level"])){
        header("refresh:0;url=../../index.php");
    }else{
        $conn = getDBConnection();
        if (empty($conn))
            die("error in app");
        else {
            $Counter = 0;
            $sql = "SELECT * FROM assort WHERE IsDel = 0 order by catName,title asc";
            $result = mysqli_query($conn, $sql);
?>
<body>
<table border="0" cellspacing="1" cellpadding="0" height="144">
    <tr bordercolor="#CC3399">
    <form action="../../index.php" method="post">
        <td width="666" align="right" colspan="5" bgcolor="#CC3399">
        <span lang="fa">
		<font face="Tahoma" size="2" color="#FFFF00">&nbsp;</font><font face="Tahoma" size="4" color="#FFFFCC">&nbsp;&nbsp;طبقه بندی مطالب&nbsp; :</font></span> 
		<p>
        <font color="#FFFF00">
		<input type="submit" value="صفحه اصلی" name="B4" style="float: left; font-family:Tahoma"/>&nbsp;&nbsp;</font></td>
	</form>
    </tr>
    <tr>
        <td width="153" align="center" colspan="3" height="19">
        &nbsp;</td>
        <td align="center" width="146" height="19">
        &nbsp;</td>
        <td align="center" width="366" height="19">
        &nbsp;</td>
    </tr>
    <tr>
        <td width="111" align="center" bgcolor="#000080" height="28" colspan="2">
        <span lang="fa"><font face="Tahoma" size="2" color="#FFFF00">ویرایش</font></span></td>
        <td align="center" bgcolor="#000080" width="41" height="28">
        <font face="Tahoma" size="2" color="#FFFF00"><span lang="fa">ردیف</span></font></td>
        <td align="center" bgcolor="#000080" height="28">
        <span lang="fa"><font face="Tahoma" size="2" color="#FFFF00">موضوع</font></span></td>
        <td align="center" bgcolor="#000080" width="366" height="28">
        <span lang="fa"><font face="Tahoma" size="2" color="#FFFF00">عنوان</font></span></td>
    </tr>
    <tr>
    <form action="../destination.php" method="post">
        <td width="111" align="center" bgcolor="#FFCC99" height="30" colspan="2">
        <input type="hidden" name="job" value="newAssort">
        <font face="Arial">
		<input type="submit" value="جدید" name="B3" style="float: center; font-family:Tahoma"/></font></td>
        <td align="center" bgcolor="#FFCC99" width="41" height="30">
        <font face="Tahoma" size="2">N/A</font></td>
        <td align="center" bgcolor="#FFCC99" height="30">
        <span lang="fa"><font face="Tahoma" size="2">
        	<select size="1" name="catName" style="font-family: Tahoma">
				<option value="IE" selected>مهندسی صنایع</option>
  				<option value="IT" >فناوری اطلاعات</option>
  				<option value="Both">مباحث مشترک</option>
			</select></font></span></td>
        <td align="center" bgcolor="#FFCC99" width="366" height="30">
        <span lang="fa"><font face="Tahoma" size="2">
        <input type="text" size="50" name="title" style="font-family: Tahoma" ></font></span></td>
    </form>
    </tr>
<?php 
    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
                $Counter = $Counter + 1;
                if ($row["catName"]=="IE"){
                	$bgColor="#FFFFCC";
                }elseif($row["catName"]=="IT"){
                	$bgColor="#CCFFFF";
                }else{
                	$bgColor="#CCFF99";
                }
    ?>
    <tr bgcolor=<?php echo $bgColor ?>>
        <td height="31" width="49">
    	   <form action="../destination.php" method="post">
        	<input type="hidden" value="delAssort" name="job"/>
        	<input type="hidden" value="<?php echo $row['ID']; ?>" name="assortID"/>
			<font face="Arial">
			<input type="submit" value="حذف" name="B2" style="float: right; font-family:Tahoma"/> <font size="2"></font>
		  </form>
		</td>
        <form action="../destination.php" method="post">
             <td height="31" width="61">
           	 <input type="hidden" value="editAssort" name="job"/>
        	 <input type="hidden" value="<?php echo $row['ID']; ?>" name="assortID"/>
			 <font face="Arial">
			 <input type="submit" value="&#1608;&#1740;&#1585;&#1575;&#1740;&#1588;" name="B2" style="float: left; font-family:Tahoma"/> <font size="2"></font>
		    </td>
            <td width="41" height="31">
            <p align="center"><font face="Arial" size="2"><?php echo $Counter; ?></font></td>
            <td height="31" align="center"><font face="Arial" size="2">
        	   <select size="1" name="catName" style="font-family: Tahoma">
				<option value="IE" <?php if ($row['catName'] == 'IE'){echo "selected";} ?>>مهندسی صنایع</option>
  				<option value="IT" <?php if ($row['catName'] == 'IT'){echo "selected";} ?>>فناوری اطلاعات</option>
  				<option value="Both" <?php if ($row['catName'] == 'Both'){echo "selected";} ?>>مباحث مشترک</option>
	           </select></font></td>
            <td width="366" height="31">
            <p align="center">
            <font face="Arial" size="2">
			<input type="text" size="50" name="title" value="<?php echo $row['title']; ?>" style="font-family: Tahoma" ></font></td>
        </form>
    </tr>
<?php
}}
?>
</table>
<?php    
}}}
?>
</body>

</html>