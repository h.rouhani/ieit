<html dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title> ویرایشگر متن</title>
    <script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
</head>

<?php
include_once "../publics.php";
session_start();
if (!isset($_SESSION["userName"])) {
    header("refresh:0;url=../login/login.html");
}
else {
    if (!isset($_SESSION["level"])) {
        header("refresh:0;url=../../index.php");
    }
    else {
        $conn = getDBConnection();
        if (empty($conn))
            die("error in app");
        else {
            $Counter = 0;
            $articleID = $_POST['articleID'];
            $sql = "SELECT * FROM articles WHERE ID = '$articleID' AND IsDel = 0";
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0) {
// output data of each row
                while ($row = mysqli_fetch_assoc($result)) {
                    $articleTitle= $row['title'];
                    $articleContent=$row['content'];
                    $catID=$row['catID'];
                }
            }
        }
        mysqli_close($conn);
    }
}
?>
<body>
<table bgcolor="#CCFFCC">

<tr>
<td bgcolor="4CAF50" align="center" height="30" width="115">
<form action="../../index.php" method="post">
<input type="submit" value="صفحه اصلی" name="B4" style="float: RIGHT; font-family:Tahoma"/></font>
</form>
	</td>
<form action="../destination.php" method="post">
    <input type="hidden" name="job" value="editArticle"/>
    <input type="hidden" name="articleID" value="<?php echo $articleID; ?>" />
<td bgcolor="#4CAF50" align="center" height="30" colspan="2">
<b>
<font face="Tahoma">ویرایشگر متن</font></b></td>

</tr>
<tr>
<td colspan="2" width="375">
&nbsp;</td>

<td width="18">
&nbsp;</td>

</tr>
<tr>
    <td width="115" bgcolor="#4CAF50"><font face="Tahoma"><span lang="fa">عنوان</span> : </font> </td>
    <td width="256">
        <font face="Tahoma">
        <select size="1" name="catID">
        <?php $conn = getDBConnection();
        if (empty($conn))
            die("error in app");
        else {
            $Counter = 0;
            $sql = "SELECT ID,title,catName FROM assort WHERE IsDel = 0 order by title asc";
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0) {
            // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
                    if ($row["ID"]==$catID){
                        echo '<option value='.$row["ID"].' selected>'.$row["title"].'</option>';
                    }else{
                        echo '<option value='.$row["ID"].'>'.$row["title"].'</option>';
                    }
                }
            }
        }
        mysqli_close($conn);
        ?>
        </select>
        </font>
    </td>
    <td width="18">&nbsp;</td>
</tr>

<tr>
    <td width="115">&nbsp;</td>
    <td width="256">&nbsp;</td>
    <td width="18">&nbsp;</td>
</tr>

<tr>
    <td width="115" bgcolor="4CAF50"><font face="Tahoma">موضوع فرعی :</font></td>
    <td width="256"><font face="Tahoma">
        <input type="text" placeholder="موضوع فرعی" name="title" value="<?php echo $articleTitle; ?>"/></font></td>
    <td width="18">&nbsp;
    </td>
</tr>
<tr>
    <td width="115">&nbsp;</td>
    <td width="256">&nbsp;</td>
    <td width="18">&nbsp;</td>
</tr>
<tr>
    <td width="115" height="19" valign="top" bgcolor="4CAF50"><font face="Tahoma">متن اصلی :</font></td>
    <td width="256" height="62" valign="top" rowspan="2"><font face="Tahoma">

        <textarea name="content" id="content" rows="1" cols="20" ><?php echo $articleContent; ?></textarea>
        <script type="text/javascript">
            CKEDITOR.replace('content', {
                width: '650px',
                height: '250px',
            });
        </script>
        </font></td>

<td width="18" height="62" valign="top" rowspan="2">&nbsp;</td>
</tr>
<tr>
    <td width="115" height="66" valign="top"><font face="Tahoma">
    <input type="submit" value="&#1584;&#1582;&#1740;&#1585;&#1607; &#1605;&#1578;&#1606;" style="font-family: Tahoma"/></font></td>
</tr>
<tr>
    <td width="115">&nbsp;</td>
    <td width="256">&nbsp;</td>
    <td width="18">&nbsp;</td>
</tr>
</form>
</table>

</body>
</html>