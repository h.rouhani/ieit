<html dir="rtl">
<head lang="fa">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>IEIT</title>

<style>
body {
  margin: 0;
  font-size: 28px;
}

.header {
  background-color: #f1f1f1;
  padding: 30px;
  text-align: center;
}

#navbar {
  overflow: hidden;
  background-color: #333;
}

#navbar a {
  float: right;
  display: block;
  color: #fff;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%
}

.sticky + .content {
  padding-top: 60px;
}
</style>
</head>
<body onscroll="myFunction()">
<?php
    session_start();
    include_once "../publics.php";

    $articleID=$_REQUEST['articleID'];
    $editUser = $_SESSION['userName'];

    $conn = getDBConnection();

    mysqli_query($conn,'SET NAMES utf8');

    $results = mysqli_query($conn, "SELECT * FROM articles WHERE IsDel = 0 and ID = '$articleID'");

        if (mysqli_num_rows($results) > 0) {
            while ($row = mysqli_fetch_assoc($results)) {
                $catID = $row['catID'];
                $title = $row['title'];
                $content = $row['content'];
            }
        }

    $results = mysqli_query($conn, "SELECT * FROM assort WHERE IsDel = 0 and ID = '$catID'");

        if (mysqli_num_rows($results) > 0) {
            while ($row = mysqli_fetch_assoc($results)) {
                $catName = $row['catName'];
                $catTitle = $row['title'];
            }
        }

    mysqli_close($conn);

    echo '<h2 align="center">'.$catTitle.'</h2>';
?>
<div id="navbar">
  <font face="Tahoma">
  <a href="../../index.php" dir="rtl"><font size="4">صفحه اصلی</font></a>
  <a href="../view.php" dir="rtl"><font size="4">صفحه قبلی</font></a>
</div>

<br/>
<div class="content">
            <div align="right">
			<table bgcolor="#CCFFCC" >
			    <form method="post" action="../destination.php">

                    <input type="hidden" value="commentArticle" name="job" />
                    <input type="hidden" value="<?php echo $articleID ; ?>" name="articleID" />
                    <input type="hidden" value="<?php echo $catID ; ?>" name="catID" />
            	<tr>
					<td><font face="Tahoma"><b>عنوان:</b></td>
					<td><b><?php echo $title ; ?></b></td>
					<td > &nbsp;</td>
                    <td > &nbsp;</td>
				</tr>
				<tr>
					<td colspan="4" bgcolor="#FFFFCC"><?php echo $content ?></td>
				</tr>
                <tr>
                    <td bgcolor="#4CAF50"><font face="Tahoma"><span lang="fa">موضوع</span> : </font> </td>
					<td colspan="3">
                        <input type="text" name="subject" placeholder="موضوع جهت ارسال" />
                    </td>
				</tr>
                <tr>
                    <td bgcolor="#4CAF50" valign="top"><font face="Tahoma"><span lang="fa">متن</span> : </font> </td>
					<td colspan="3">
                        <textarea name="comment" id="comment" rows="8" cols="40" placeholder="متن مورد نظر جهت ارسال"></textarea>
                    </td>
				</tr>
                <tr>
					<td>

                    </td>
                    <td>

                    </td>
                    <td colspan="2">
                        <input type="submit" value="ارسال نظر"name="sbmComment"/>
                    </td>
				</tr>
                </form>
			</table>
			</div>


</div>

<script>
var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>
   
</body>
</html>'

 
