<?php

function getDBConnection()
{
    $connection = new mysqli("localhost", "root","", "ieit");
    if ($connection->connect_error)
        return null;
    else {
        mysqli_query($connection, "set names utf8");
        return $connection;
    }

}